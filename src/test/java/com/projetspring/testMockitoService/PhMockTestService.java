
package com.projetspring.testMockitoService;


import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Repository.PersonnePhisiqueRepository;
import com.projetspring.service.AppelOffreService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
    public class PhMockTestService {

        @Mock
        private PersonnePhisiqueRepository personnePhisiqueRepository;
        @InjectMocks
        private AppelOffreService as;
        PersonnePhisique p1 = PersonnePhisique.builder()
                .id(29)
                .nom("amira")
                .prenom("ali")
                .adresse("ndb")
                .telephone("22365687")
                .mail("amira@gmail.com")
                .build();

        @Test
        public void createPH() {

            given(personnePhisiqueRepository.save(p1)).willReturn(p1);
            PersonnePhisique p2 = as.saveph(p1);
            Assertions.assertThat(p2).isNotNull();

        }

        PersonnePhisique p0 = PersonnePhisique.builder()
                .id(2)
                .nom("amira")
                .prenom("ali")
                .adresse("ndb")
                .telephone("22365687")
                .mail("amira@gmail.com")
                .build();

        @Test
        public void findAllph() {
            given(personnePhisiqueRepository.findAll()).willReturn(List.of(p1, p0));
            List<PersonnePhisique> phList = as.findAllPP();
            Assertions.assertThat(phList).isNotNull();
            Assertions.assertThat(phList.size()).isEqualTo(2);
        }

        @Test
        public void findOneph() {
            Integer id=29;
            given(personnePhisiqueRepository.findById(id)).willReturn(Optional.of(p1));
            PersonnePhisique pp= as.getpersonneph(id);
            Assertions.assertThat(pp).isNotNull();
            Assertions.assertThat(pp.getNom()).isEqualTo("amira");
        }


        @Test
        public void updatePH() {


           given(personnePhisiqueRepository.save(p0)).willReturn(p0);


            PersonnePhisique updatedph = as.updatePH(p0, 2);
            p0.setMail("mourad@gmail.com");
            p0.setPrenom("mourad");
            Assertions.assertThat(updatedph.getMail()).isEqualTo("mourad@gmail.com");
            Assertions.assertThat(updatedph.getPrenom()).isEqualTo("mourad");

        }

        @Test
        public void deletepp() {
            Integer Id = 1;

            willDoNothing().given(personnePhisiqueRepository).deleteById(Id);
            as.deletePP(Id);

            verify(personnePhisiqueRepository, times(1)).deleteById(Id);


        }
    }
