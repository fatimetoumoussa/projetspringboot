package com.projetspring.testMockito;

import com.projetspring.Controller.AppelOffreController;
import com.projetspring.Model.PersonneMorale;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.service.AppelOffreService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@ContextConfiguration(classes = {AppelOffreService.class, AppelOffreController.class})
@WebMvcTest
public class PersonneMMockitotest {


    @Autowired
    MockMvc mkvc;
    @MockBean
    AppelOffreService appelOffreService;
    //@Mock
    PersonnePhisique ph;
    private PersonneMorale pm;
    private List<PersonneMorale> listpm=new ArrayList<>();



    @Test
    public void getallpm() throws Exception {
        pm=new PersonneMorale(1,"kkk","ff",null,null);
        listpm.add(pm);
        when(appelOffreService.findAllPM()).thenReturn(listpm);
        mkvc.perform(get("/findAllPM")).andExpect(status().isOk()).andDo(print());

    }




    @Test
    public void getpm() throws Exception {
        ph=new PersonnePhisique(1,"nnn","sss","sss","sss","sss",null);
        pm=new PersonneMorale(1,"kkk","ff",null,ph);

        ResponseEntity<PersonneMorale> pms = new ResponseEntity<>(pm, HttpStatus.OK);
        when(appelOffreService.getpersonnepm(1)).thenReturn(pms);
        mkvc.perform(get("/getPM/1")).andExpect(status().isOk()).andDo(print());

    }




    @Test
    public void postpm() throws Exception {
       // {"id":1,"denomination":"denomination","NumIm":"NumIm","Representantlegale":null}
        mkvc.perform(post("/savePM").content(" {\"id\":1,\"denomination\":\"denomination\",\"NumIm\":\"NumIm\",\"Representantlegale\":null}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated()).andDo(print());

    }

    @Test
    public void deletepm() throws Exception {
        when(appelOffreService.deletePM(1)).thenReturn(null);
        mkvc.perform(delete("/deletePM/1")).andExpect(status().isOk()).andDo(print());
    }




    @Test
    public void updatepm() throws Exception {

        mkvc.perform(put("/updatePM/1").
                content(" {\"id\":1,\"denomination\":\"denomination\",\"NumIm\":\"NumIm\",\"Representantlegale\":null}").
                contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated()).andDo(print());
    }


}
