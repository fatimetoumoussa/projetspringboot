package com.projetspring.testMockito;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projetspring.Controller.AppelOffreController;

import com.projetspring.Model.AppelOffre;
import com.projetspring.Model.PersonneMorale;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Repository.AppelOffreRepository;
import com.projetspring.Repository.PersonnePhisiqueRepository;

import com.projetspring.service.AppelOffreService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {AppelOffreService.class, AppelOffreController.class})
@WebMvcTest
public class AppelOffreMockTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    private AppelOffreService appelOffreService;
    @Mock
    private AppelOffreRepository appelOffreRepository;

    PersonnePhisique ph = PersonnePhisique.builder()
            .id(29)
            .nom("amira")
            .prenom("ali")
            .adresse("ndb")
            .telephone("22365687")
            .mail("amira@gmail.com")
            .build();

    PersonneMorale pm = PersonneMorale.builder()
            .id(28)
            .denomination("CDI")
            .NumIm("7866")
            .Representantlegale(ph)
            .build();

    private List<AppelOffre> listao = new ArrayList<>();
    String dp = "2022-02-03";
    String df = "2022-03-10";
    LocalDate dap = LocalDate.parse(dp);
    LocalDate daf = LocalDate.parse(df);
    AppelOffre ao = AppelOffre.builder()
            .id(14)
            .datepublication(dap)
            .datefin(daf)
            .delai(45.0)
            .objet("projet")
            .montant(56000.0F)
            .PH(ph)
            .PM(pm)
            .build();

    ObjectMapper objectMapper;

    @Test
    public void findAllTest() throws Exception{

        listao.add(ao);

        when(appelOffreService.findAllAO()).thenReturn(listao);
        mockMvc.perform(get("/findAllAO")).andExpect(status().isOk()).andDo(print());

    }

    @Test
    public void finOneTest() throws Exception {
        int id=ao.getId();
        when(appelOffreService.getAppelOffre(id)).thenReturn(ao);
        mockMvc.perform(get("/getAppelOffre/14")).andExpect(status().isOk()).andDo(print());
    }
    @Test
    public void testcreateAO() throws Exception {
        mockMvc.perform(post("/saveAO").content("{\n" +
                "    \"id\": 45,\n" +
                "    \"objet\": \"projet\",\n" +
                "    \"montant\": 4500.0,\n" +
                "    \"datepublication\": \"2022-03-03\",\n" +
                "    \"datefin\": \"2022-04-03\",\n" +
                "    \"delai\": 43,\n" +
                "    \"ph\": null,\n" +
                "    \"pm\": null\n" +
                "}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andDo(print());

    }

    @Test
    public void updateao() throws Exception {

        mockMvc.perform(put("/updateAO/1").content("{\n" +
                "    \"id\": 45,\n" +
                "    \"objet\": \"projet\",\n" +
                "    \"montant\": 4500.0,\n" +
                "    \"datepublication\": \"2022-03-03\",\n" +
                "    \"datefin\": \"2022-04-03\",\n" +
                "    \"delai\": 43,\n" +
                "    \"ph\": null,\n" +
                "    \"pm\": null\n" +
                "}"
        ).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andDo(print());
    }


    @Test
    public void deleteao() throws Exception {
        when(appelOffreService.deleteAO(1)).thenReturn(null);
        mockMvc.perform(delete("/deleteAO/1")).andExpect(status().isGone()).andDo(print());
    }

}

