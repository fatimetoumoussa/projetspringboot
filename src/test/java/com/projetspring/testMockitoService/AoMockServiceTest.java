package com.projetspring.testMockitoService;

import com.projetspring.Model.AppelOffre;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Repository.AppelOffreRepository;
import com.projetspring.service.AppelOffreService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AoMockServiceTest {
    @Mock
    private AppelOffreRepository aor;
    @InjectMocks
    private AppelOffreService as;

    PersonnePhisique p1 = PersonnePhisique.builder()
            .id(29)
            .nom("amira")
            .prenom("ali")
            .adresse("ndb")
            .telephone("22365687")
            .mail("amira@gmail.com")
            .build();
    PersonnePhisique p0 = PersonnePhisique.builder()
            .id(2)
            .nom("amira")
            .prenom("ali")
            .adresse("ndb")
            .telephone("22365687")
            .mail("amira@gmail.com")
            .build();

    AppelOffre ao1 = AppelOffre.builder()
            .id(1)
            .objet("projet")
            .montant(7888F)
            .delai(56.0)
            .datepublication(LocalDate.parse("2022-04-22"))
            .datefin(LocalDate.parse("2022-05-22"))
            .PH(p1)
            .build();

    AppelOffre ao2 = AppelOffre.builder()
            .id(2)
            .objet("projet")
            .montant(1888F)
            .delai(58.0)
            .datepublication(LocalDate.parse("2022-04-28"))
            .datefin(LocalDate.parse("2022-05-29"))
            .PH(p0)
            .build();

    @Test
    public void createAO() {

        given(aor.save(ao1)).willReturn(ao1);
        AppelOffre appelOffre = as.saveAO(ao1);
        Assertions.assertThat(appelOffre).isNotNull();

    }


    @Test
    public void findAllao() {
        given(aor.findAll()).willReturn(List.of(ao1, ao2));
        List<AppelOffre> aoList = as.findAllAO();
        Assertions.assertThat(aoList).isNotNull();
        Assertions.assertThat(aoList.size()).isEqualTo(2);
    }

    @Test
    public void findOneAO() {
        Integer id=29;
        given(aor.findById(id)).willReturn(Optional.of(ao1));
        AppelOffre appelOffre = as.getAppelOffre(id);
        Assertions.assertThat(appelOffre).isNotNull();
        Assertions.assertThat(appelOffre.getPH()).isEqualTo(p1);
    }


    @Test
    public void updateAO() {
      //  given(aor.save(ao1)).willReturn(ao1);
        ao1.setMontant(679F);
        ao1.setDelai(80.0);

        AppelOffre updatedao = as.updateAo(ao1,1);

        Assertions.assertThat(updatedao.getMontant()).isEqualTo(679F);
        Assertions.assertThat(updatedao.getDelai()).isEqualTo(80.0);
    }

    @Test
    public void deleteao() {
        // given - precondition or setup
        Integer Id = 1;

        willDoNothing().given(aor).deleteById(Id);

        // when -  action or the behaviour that we are going test

        as.deleteAO(Id);

        // then - verify the output
        verify(aor, times(1)).deleteById(Id);
    }


}

