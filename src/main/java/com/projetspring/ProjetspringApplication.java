package com.projetspring;

import com.projetspring.Model.PersonneMorale;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ProjetspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetspringApplication.class, args);
	}
}
