package com.projetspring.CrudTest;

import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Repository.PersonnePhisiqueRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import javax.persistence.Table;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DataJpaTest
public class PersonnePhisiqueTest {


    @Autowired
    PersonnePhisiqueRepository phr;
    @Test
    @Rollback(value = false)
    @Order(1)
    public void  savepersonnephisique(){
      // PersonnePhisique ph=new PersonnePhisique(100,"jj","kk","oo","gg","yy",null);

        PersonnePhisique ph=  PersonnePhisique.builder()

                .adresse("rue404").mail("@gmail.com").nom("p1").prenom("p1p1").telephone("443322")

                .build();

        phr.save(ph);
        Assertions.assertThat(ph.getId()).isGreaterThan(0);
    }
    @Test
   //@Rollback(value = false)
    @Order(2)
    public void getpersonnephisique(){
PersonnePhisique ph =phr.findById(1).get();
        Assertions.assertThat(ph.getId()).isEqualTo(1);
    }
    @Test
   //  @Rollback(value = false)
    @Order(3)
    public void getallpersonnephisique(){
        List<PersonnePhisique> phl =phr.findAll();
        Assertions.assertThat(phl.size()).isGreaterThan(1);
    }
    @Test
    @Rollback(value = false)
    @Order(4)
    public void updatepersonnephisique(){
        PersonnePhisique ph =phr.findById(1).get();
        ph.setNom("p2");
        PersonnePhisique phu = phr.save(ph);
        Assertions.assertThat(phu.getNom()).isEqualTo("p2");
}
@Test
@Rollback(value = false)
@Order(5)
    public void deletePersonnePhisique(){

     phr.deleteById(1);

    Assertions.assertThat(phr.findById(1)).isEmpty();

}
}
