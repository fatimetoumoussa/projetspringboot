package com.projetspring.testMockito;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projetspring.Controller.AppelOffreController;

import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Repository.PersonnePhisiqueRepository;

import com.projetspring.service.AppelOffreService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.web.bind.annotation.RequestMapping;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {AppelOffreService.class, AppelOffreController.class})
@WebMvcTest

public class PersonphiMockTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private AppelOffreService personnePhysiqueService;
    @Mock
    private PersonnePhisiqueRepository pr;
    private List<PersonnePhisique> listpp = new ArrayList<>();

    PersonnePhisique p1 = PersonnePhisique.builder()
            .id(29)
            .nom("amira")
            .prenom("ali")
            .adresse("ndb")
            .telephone("22365687")
            .mail("amira@gmail.com")
            .build();




    ObjectMapper objectMapper;

    @Test
    public void findAllTest() throws Exception{

        listpp.add(p1);

      when(personnePhysiqueService.findAllPP()).thenReturn(listpp);
        mockMvc.perform(get("/findAllPP")).andExpect(status().isOk()).andDo(print());

    }
    @Test
    public void finOneTest() throws Exception {
        int id=p1.getId();
        when(personnePhysiqueService.getpersonneph(id)).thenReturn(p1);
        mockMvc.perform(get("/getpersonnePH/29")).andExpect(status().isOk()).andDo(print());
    }

    @Test
    public void testcreatePH() throws Exception {

        mockMvc.perform(post("/savePP")
                .content("{\n" +
                        "    \"id\": 29,\n" +
                        "    \"nom\": \"sima\",\n" +
                        "    \"prenom\": \"khali\",\n" +
                        "    \"adresse\": \"nktt\",\n" +
                        "    \"telephone\": \"46573434\",\n" +
                        "    \"mail\": \"sami@gmail.com\",\n" +
                        "    \"reference\": null,\n" +
                        "    \"r\": null\n" +
                        "}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andDo(print());

    }


    @Test
    public void deleteph() throws Exception {
        when(personnePhysiqueService.deletePP(1)).thenReturn(null);
        mockMvc.perform(delete("/deletePP/1")).andExpect(status().isGone()).andDo(print());
    }


    @Test
    public void updateph() throws Exception {

        mockMvc.perform(put("/updatePP/1").content("{\n" +
                "    \"id\": 29,\n" +
                "    \"nom\": \"sima\",\n" +
                "    \"prenom\": \"khali\",\n" +
                "    \"adresse\": \"nktt\",\n" +
                "    \"telephone\": \"46573434\",\n" +
                "    \"mail\": \"sami@gmail.com\",\n" +
                "    \"reference\": null,\n" +
                "    \"r\": null\n" +
                "}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andDo(print());
    }

}


