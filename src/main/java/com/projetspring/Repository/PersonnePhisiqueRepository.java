package com.projetspring.Repository;

import com.projetspring.Model.PersonnePhisique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonnePhisiqueRepository extends JpaRepository<PersonnePhisique,Integer> {
    PersonnePhisique findByNom(String pp);
}
