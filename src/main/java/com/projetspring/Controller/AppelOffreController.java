package com.projetspring.Controller;



import com.projetspring.Model.*;
import com.projetspring.service.AppelOffreService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
    public class AppelOffreController {
      @Autowired
      private AppelOffreService serviceappeloffre;


        @PostMapping("/savePP")
        public PersonnePhisique placeRef(@RequestBody PersonnePhisique pp) {
            return serviceappeloffre.saveph(pp);
        }
        @GetMapping("/getpersonnePH/{id}")
        public PersonnePhisique getpersonneph(@PathVariable Integer id){
         return serviceappeloffre.getpersonneph(id);
        }
        @GetMapping("/findAllPP")
        public List<PersonnePhisique> findAllPP(){
            return serviceappeloffre.findAllPP();
        }

        @PutMapping("/updatePP/{id}")
        public ResponseEntity<PersonnePhisique> updatePH(@RequestBody PersonnePhisique ph, @PathVariable() Integer id) {


                PersonnePhisique phu = serviceappeloffre.updatePH(ph, id);
                return new ResponseEntity<>(phu,HttpStatus.ACCEPTED);


        }


        @DeleteMapping("/deletePP/{id}")
        public ResponseEntity<HttpStatus> deletePP(@PathVariable("id") Integer id) {

                serviceappeloffre.deletePP(id);
                return new ResponseEntity<>(null,HttpStatus.GONE);

        }





        //
        //
        // ============ reference =============

        @DeleteMapping("/deleteRef/{id}")
        public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") Integer id) {
         return  serviceappeloffre.deletePM(id);
        }
        @GetMapping("/getRef/{id}")
        public Reference getRef(@PathVariable Integer id){
      return serviceappeloffre.getRef(id);
        }
        @PutMapping("/updateRef/{id}")
        public Reference updatePH(@RequestBody Reference ref, @PathVariable Integer id) {return serviceappeloffre.updater(ref,id);
        }
        @PostMapping("/saveRef")
        public ResponseEntity<Reference> saveR(@RequestBody Reference reference){
            serviceappeloffre.saveR(reference);
            return new ResponseEntity<>(reference,HttpStatus.CREATED);
        }

        @GetMapping("/findAllRef")
        public List<Reference> findAllRef(){
            return serviceappeloffre.findAllRef();
        }






        // =============Appele doffre=======



        @GetMapping("/getAppelOffre/{id}")
        public AppelOffre getAppelOffre(@PathVariable Integer id){
         return   serviceappeloffre.getAppelOffre(id);
        }
    @PostMapping("/saveAO")
        public AppelOffre saveAO(@RequestBody AppelOffre ao){
            return serviceappeloffre.saveAO(ao);
        }

        @PutMapping("/updateAO/{id}")
        public ResponseEntity<Object> updatePH(@RequestBody AppelOffre ao, @PathVariable Integer id) {

           // Optional<Reference> phOptional = referenceRepository.findById(id);

                AppelOffre up = serviceappeloffre.updateAo(ao, id);
                return new ResponseEntity<>(up, HttpStatus.ACCEPTED);

        }
        @GetMapping("/findAllAO")
        public List<AppelOffre> findAllAO(){
            return serviceappeloffre.findAllAO();
        }
        @DeleteMapping("/deleteAO/{id}")
        public ResponseEntity<HttpStatus> deleteAO(@PathVariable("id") Integer id) {

                serviceappeloffre.deleteAO(id);
                return new ResponseEntity<>(null,HttpStatus.GONE);

        }





        // ====== personne morale======


        @GetMapping("/findAllPM")
        public List<PersonneMorale> findAllPM(){
            return serviceappeloffre.findAllPM();
        }

        @DeleteMapping("/deletePM/{id}")
        public ResponseEntity<HttpStatus> deletePM(@PathVariable("id") Integer id) {
          return serviceappeloffre.deletePM(id);
        }
        @PostMapping("/savePM")
        public ResponseEntity<PersonneMorale> savepm(@RequestBody PersonneMorale personneMorale){
            serviceappeloffre.savepm(personneMorale);
            return  new ResponseEntity<>(personneMorale,HttpStatus.CREATED);
        }
        @PutMapping("/updatePM/{id}")
        public ResponseEntity<Object> updatePH(@RequestBody PersonneMorale pm, @PathVariable Integer id) {

            serviceappeloffre.updatePm(pm,id);
            return  new ResponseEntity<>(pm,HttpStatus.CREATED);
        }
        @GetMapping("/getPM/{id}")
        public ResponseEntity<PersonneMorale> getpersonnepm(@PathVariable Integer id){
         return  serviceappeloffre.getpersonnepm(id);
        }





        //user


    @GetMapping("/findAllu")
    public List<utilisateur> findAllu(){
        return serviceappeloffre.findAllu();
    }

    @DeleteMapping("/deleteu/{id}")
    public ResponseEntity<HttpStatus> deleteu(@PathVariable("id") Integer id) {
        return serviceappeloffre.deleteu(id);
    }
    @PostMapping("/saveu")
    public ResponseEntity<utilisateur> saveu(@RequestBody utilisateur u){
        serviceappeloffre.saveu(u);
        return  new ResponseEntity<>(u,HttpStatus.CREATED);
    }
    @PutMapping("/updateu/{id}")
    public ResponseEntity<Object> updateu(@RequestBody utilisateur u, @PathVariable Integer id) {

        serviceappeloffre.updateu(u,id);
        return  new ResponseEntity<>(u,HttpStatus.CREATED);
    }
    @GetMapping("/getu/{id}")
    public utilisateur getu(@PathVariable Integer id){
        return  serviceappeloffre.getu(id);
    }


















}
