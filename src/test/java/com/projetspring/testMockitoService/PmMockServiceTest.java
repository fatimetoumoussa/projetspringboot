package com.projetspring.testMockitoService;


import com.projetspring.Model.PersonneMorale;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Repository.PersonneMoraleRepository;
import com.projetspring.service.AppelOffreService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class PmMockServiceTest {
    @Mock
    private PersonneMoraleRepository pmr;
    @InjectMocks
    private AppelOffreService as;

    PersonnePhisique p1 = PersonnePhisique.builder()
            .id(1)
            .nom("amira")
            .prenom("ali")
            .adresse("ndb")
            .telephone("22365687")
            .mail("amira@gmail.com")
            .build();

    PersonneMorale pm = PersonneMorale.builder()
            .id(1)
            .denomination("nktt")
            .NumIm("879")
            .Representantlegale(p1)
            .build();

    PersonneMorale pm1 = PersonneMorale.builder()
            .id(2)
            .denomination("nktt")
            .NumIm("8111")
            .Representantlegale(p1)
            .build();
    @Test
    public void createPM() {

        given(pmr.save(pm)).willReturn(pm);
        PersonneMorale pm2 = as.savepm(pm);
        Assertions.assertThat(pm2).isNotNull();

    }
    @Test
    public void findAllpm() {
        given(pmr.findAll()).willReturn(List.of(pm, pm1));
        List<PersonneMorale> pmList = as.findAllPM();
        Assertions.assertThat(pmList).isNotNull();
        Assertions.assertThat(pmList.size()).isEqualTo(2);
    }

    @Test
    public void findOnepm() {
        Integer id=1;
        given(pmr.findById(id)).willReturn(Optional.of(pm));
        ResponseEntity<PersonneMorale> personneMorale = as.getpersonnepm(id);
        Assertions.assertThat(personneMorale).isNotNull();
        Assertions.assertThat(personneMorale.getStatusCode()).isEqualTo(HttpStatus.FOUND);
    }
    @Test
    public void updatePH() {
        given(pmr.save(pm)).willReturn(pm);

        pm.setDenomination("ndb");
        pm.setNumIm("555");

        PersonneMorale updatedpm = as.updatePm(pm, 1);

        Assertions.assertThat(updatedpm.getDenomination()).isEqualTo("ndb");
        Assertions.assertThat(updatedpm.getNumIm()).isEqualTo("555");
    }

    @Test
    public void deletepm() {
        // given - precondition or setup
        Integer Id = 1;

        willDoNothing().given(pmr).deleteById(Id);

        // when -  action or the behaviour that we are going test

        as.deletePM(Id);

        // then - verify the output
        verify(pmr, times(1)).deleteById(Id);
    }

}
