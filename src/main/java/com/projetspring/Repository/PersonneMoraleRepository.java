package com.projetspring.Repository;

import com.projetspring.Model.PersonneMorale;
import com.projetspring.Model.PersonnePhisique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonneMoraleRepository extends JpaRepository<PersonneMorale,Integer> {

    PersonneMorale findByDenomination(String pp);
}
