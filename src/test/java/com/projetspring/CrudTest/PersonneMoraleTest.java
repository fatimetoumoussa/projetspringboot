package com.projetspring.CrudTest;

import com.projetspring.Model.PersonneMorale;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Repository.PersonneMoraleRepository;
import com.projetspring.Repository.PersonnePhisiqueRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PersonneMoraleTest {


    @Autowired
    PersonneMoraleRepository pmr;
    @Test
    @Rollback(value = false)
    @Order(1)
    public void  savepersonnemorale(){
        PersonneMorale pm=PersonneMorale.builder().denomination("kkkk").NumIm("jjjj").build();

        pmr.save(pm);
        Assertions.assertThat(pm.getId()).isGreaterThan(0);
    }
    @Test
    //@Rollback(value = false)
    @Order(2)
    public void getpersonnemorale(){
        PersonneMorale ph =pmr.findById(1).get();
        Assertions.assertThat(ph.getId()).isEqualTo(1);
    }
    @Test
    //  @Rollback(value = false)
    @Order(3)
    public void getallpersonnemoralee(){
        List<PersonneMorale> phl =pmr.findAll();
        Assertions.assertThat(phl.size()).isGreaterThan(1);
    }

    @Test
    @Rollback(value = false)
    @Order(4)
    public void updatepersonnemorale(){
        PersonneMorale ph =pmr.findById(1).get();
        ph.setNumIm("ni6615");
        PersonneMorale pmu = pmr.save(ph);
        Assertions.assertThat(pmu.getNumIm()).isEqualTo("ni6615");
    }
    @Test
    @Rollback(value = false)
    @Order(5)
    public void deletePersonnemorale(){

        pmr.deleteById(1);

        Assertions.assertThat(pmr.findById(1)).isEmpty();

    }
}
