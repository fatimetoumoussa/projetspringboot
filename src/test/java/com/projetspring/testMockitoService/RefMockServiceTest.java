package com.projetspring.testMockitoService;


import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Model.Reference;
import com.projetspring.Repository.ReferenceRepository;
import com.projetspring.service.AppelOffreService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class RefMockServiceTest {

    @Mock
    private ReferenceRepository referenceRepository;
    @InjectMocks
    private AppelOffreService as;

    PersonnePhisique p1 = PersonnePhisique.builder()
            .id(29)
            .nom("amira")
            .prenom("ali")
            .adresse("ndb")
            .telephone("22365687")
            .mail("amira@gmail.com")
            .build();

    Reference ref1 = Reference.builder()
            .id(1)
            .objet("proj")
            .montant(690.0F)
            .entite("cdi")
            .datereference(LocalDate.parse("2022-03-28"))
            .pp(p1)
            .build();

    Reference ref2 = Reference.builder()
            .id(2)
            .objet("proj")
            .montant(6940.0F)
            .entite("cdi")
            .datereference(LocalDate.parse("2022-03-28"))
            .pp(p1)
            .build();

    @Test
    public void createRef() {

        given(referenceRepository.save(ref1)).willReturn(ref1);
        Reference reference = as.saveR(ref1);
        Assertions.assertThat(reference).isNotNull();

    }

    PersonnePhisique p0 = PersonnePhisique.builder()
            .id(2)
            .nom("amira")
            .prenom("ali")
            .adresse("ndb")
            .telephone("22365687")
            .mail("amira@gmail.com")
            .build();

    @Test
    public void findAllao() {
        given(referenceRepository.findAll()).willReturn(List.of(ref1, ref2));

        List<Reference> refList = as.findAllRef();
        Assertions.assertThat(refList).isNotNull();
        Assertions.assertThat(refList.size()).isEqualTo(2);
    }

    @Test
    public void findOneRef() {
        Integer id=1;
        given(referenceRepository.findById(id)).willReturn(Optional.of(ref1));
        Reference reference = as.getRef(id);
        Assertions.assertThat(reference).isNotNull();
        Assertions.assertThat(reference.getObjet()).isEqualTo("proj");
    }
//

    @Test
    public void updateAO() {
        given(referenceRepository.save(ref1)).willReturn(ref1);

        ref1.setEntite("SNIM");
        ref1.setObjet("projet");

        Reference updatedref = as.updater(ref1, 1);

        Assertions.assertThat(updatedref.getEntite()).isEqualTo("SNIM");
        Assertions.assertThat(updatedref.getObjet()).isEqualTo("projet");
    }
    //
    @Test
    public void deleteRef() {
        // given - precondition or setup
        Integer Id = 1;

        willDoNothing().given(referenceRepository).deleteById(Id);

        as.deletereference(Id);

        // then - verify the output
        verify(referenceRepository, times(1)).deleteById(Id);
    }


}


