package com.projetspring.testMockitoService;

import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Model.utilisateur;
import com.projetspring.Repository.PersonnePhisiqueRepository;
import com.projetspring.Repository.UtilisateurRepository;
import com.projetspring.service.AppelOffreService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
@ExtendWith(MockitoExtension.class)
public class utilisateurMockServiceTest {

    @Mock
    private UtilisateurRepository utilisateurRepository;
    @InjectMocks
    private AppelOffreService as;

    utilisateur u1 = utilisateur.builder()
            .id(29)
            .email("www").password("222222").build();

    @Test
    public void createu() {

        given(utilisateurRepository.save(u1)).willReturn(u1);
        utilisateur p2 = as.saveu(u1);
        Assertions.assertThat(p2).isNotNull();

    }


    utilisateur u0 = utilisateur.builder()
            .id(29)
            .email("@www").password("7777").build();

    @Test
    public void findAllu() {
        given(utilisateurRepository.findAll()).willReturn(List.of(u1, u0));
        List<utilisateur> uList = as.findAllu();
        Assertions.assertThat(uList).isNotNull();
        Assertions.assertThat(uList.size()).isEqualTo(2);
    }

    @Test
    public void findOneu() {
        Integer id=29;
        given(utilisateurRepository.findById(id)).willReturn(Optional.of(u1));
        utilisateur u= as.getu(id);
        Assertions.assertThat(u).isNotNull();
        Assertions.assertThat(u.getEmail()).isEqualTo("www");
    }


    @Test
    public void updateu() {


        given(utilisateurRepository.save(u0)).willReturn(u0);


        utilisateur updatedu = as.updateu(u0, 2);
        Assertions.assertThat(updatedu.getEmail()).isEqualTo("@www");
        Assertions.assertThat(updatedu.getPassword()).isEqualTo("7777");

    }

    @Test
    public void deleteu() {
        Integer Id = 1;

        willDoNothing().given(utilisateurRepository).deleteById(Id);
        as.deleteu(Id);

        verify(utilisateurRepository, times(1)).deleteById(Id);


    }
}
