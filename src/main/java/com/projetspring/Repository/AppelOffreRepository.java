package com.projetspring.Repository;

import com.projetspring.Model.AppelOffre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppelOffreRepository extends JpaRepository<AppelOffre,Integer> {
}
