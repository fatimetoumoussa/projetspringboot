package com.projetspring.testMockito;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projetspring.Controller.AppelOffreController;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Model.utilisateur;
import com.projetspring.Repository.PersonnePhisiqueRepository;
import com.projetspring.Repository.UtilisateurRepository;
import com.projetspring.service.AppelOffreService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@ContextConfiguration(classes = {AppelOffreService.class, AppelOffreController.class})
@WebMvcTest

public class UtilisateurMockTest {



    @Autowired
    MockMvc mockMvc;
    @MockBean
    private AppelOffreService userservice;
    @Mock
    private UtilisateurRepository utilisateurRepository;
    private List<utilisateur> listu = new ArrayList<>();

    utilisateur u1 = utilisateur.builder()
            .id(29)
            .email("www").password("222222").build();




    ObjectMapper objectMapper;

    @Test
    public void findAllTest() throws Exception{

        listu.add(u1);

        when(userservice.findAllu()).thenReturn(listu);
        mockMvc.perform(get("/findAllu")).andExpect(status().isOk()).andDo(print());

    }
    @Test
    public void finOneTest() throws Exception {
        int id=u1.getId();
        when(userservice.getu(id)).thenReturn(u1);
        mockMvc.perform(get("/getu/29")).andExpect(status().isOk()).andDo(print());
    }

    @Test
    public void testcreateu() throws Exception {

//        {"id":1,
//                "email":"www",
//                "password":"5555","r":0}

        mockMvc.perform(post("/saveu")
                .content("   {\"id\":1,\n" +
                        "                \"email\":\"www\", \n" +
                        "                \"password\":\"5555\",\"r\":0}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated()).andDo(print());

    }


    @Test
    public void deleteu() throws Exception {
        when(userservice.deleteu(1)).thenReturn(null);
        mockMvc.perform(delete("/deleteu/1")).andExpect(status().isOk()).andDo(print());
    }


    @Test
    public void updateu() throws Exception {

        mockMvc.perform(put("/updateu/1").content("   {\"id\":1,\n" +
                "                \"email\":\"www\", \n" +
                "                \"password\":\"11111111111\",\"r\":0}").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated()).andDo(print());
    }
}
