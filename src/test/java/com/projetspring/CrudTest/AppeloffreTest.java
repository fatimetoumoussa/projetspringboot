package com.projetspring.CrudTest;

import com.projetspring.Model.AppelOffre;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Repository.AppelOffreRepository;
import com.projetspring.Repository.PersonnePhisiqueRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AppeloffreTest {

    @Autowired
    AppelOffreRepository ar;
    @Autowired
    PersonnePhisiqueRepository phr;
    // PersonneMoraleRepository pmr;
    @Test
    @Rollback(value = false)
    @Order(1)
    public void  saveAppelOffre(){
        PersonnePhisique ph = phr.findById(1).get();
        // PersonneMorale pm = pmr.findById(1).get();

//         java.util.Date a=new java.util.Date("2022/05/12");
//         java.util.Date b=new java.util.Date("2024/08/22");
//         double de = b.getTime() -a.getTime();

                    AppelOffre appo = AppelOffre.builder().datepublication(LocalDate.parse("2022-04-03"))
                            .datefin(LocalDate.parse("2022-04-22"))
                            .delai(15.0)
                            .objet("societe")
                            .PH(ph).montant(4444F)
                            .PM(null).build();

                    ar.save(appo);
                    Assertions.assertThat(appo.getId()).isGreaterThan(0);

    }
    @Test
    //@Rollback(value = false)
    @Order(2)
    public void getAppelOffre(){
        AppelOffre appo =ar.findById(1).get();
        Assertions.assertThat(appo.getId()).isEqualTo(1);
    }
    @Test
    //  @Rollback(value = false)
    @Order(3)
    public void getallAppelOffre(){
        List<AppelOffre> appo =ar.findAll();
        Assertions.assertThat(appo.size()).isGreaterThan(1);
    }

    @Test
    @Rollback(value = false)
    @Order(4)
    public void updateAppelOffre(){
        AppelOffre appo =ar.findById(1).get();
       appo.setMontant(544F); ;
        AppelOffre au = ar.save(appo);
        Assertions.assertThat(au.getMontant()).isEqualTo(544F);
    }
    @Test
    @Rollback(value = false)
    @Order(5)
    public void deleteAppelOffre(){

        ar.deleteById(1);

        Assertions.assertThat(ar.findById(1)).isEmpty();

    }
}
