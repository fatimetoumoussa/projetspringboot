package com.projetspring.Repository;

import com.projetspring.Model.utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import java.util.List;

@Repository
public interface UtilisateurRepository extends JpaRepository<utilisateur,Integer> {
public utilisateur findByEmail(String email);
    public utilisateur findByPassword(String password);

}
