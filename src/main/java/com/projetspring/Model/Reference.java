package com.projetspring.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Reference {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String objet;
    private String entite;
    private LocalDate datereference;
    private float montant;
    @ManyToOne
    @JoinColumn(name = "idph")
    private PersonnePhisique pp;
    @ManyToOne
    @JoinColumn(name = "idpm")
    private PersonneMorale pm;


}
