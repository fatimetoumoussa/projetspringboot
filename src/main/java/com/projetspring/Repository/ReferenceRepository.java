package com.projetspring.Repository;

import com.projetspring.Model.PersonneMorale;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Model.Reference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface ReferenceRepository extends JpaRepository<Reference, Integer> {
    //public List<Reference> findByPp(Integer Pp);
    public List<Reference> findAllByPp(Optional<PersonnePhisique> pp);
    public List<Reference> findAllByPm(Optional<PersonneMorale> pm);
}
