package com.projetspring.Model;

import com.projetspring.Interface.Personne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppelOffre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
     private int id;
    private String objet;
    private Float montant;
    private LocalDate datepublication;
    private LocalDate datefin;

    private Double delai;
    @OneToOne(targetEntity = PersonnePhisique.class)
    @JoinColumn(name = "PersonneP_id",referencedColumnName = "id")
    private PersonnePhisique PH;
    @OneToOne(targetEntity = PersonneMorale.class)
    @JoinColumn(name = "PersonneM_id",referencedColumnName = "id")
    private PersonneMorale PM;
}
