package com.projetspring.CrudTest;

import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Model.utilisateur;
import com.projetspring.Repository.PersonnePhisiqueRepository;
import com.projetspring.Repository.UtilisateurRepository;
import org.apache.catalina.User;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class utilisateurTest {


    @Autowired
    UtilisateurRepository ur;
    @Test
    @Rollback(value = false)
    @Order(1)
    public void  saveu(){


        utilisateur u=utilisateur.builder().email("ll@ff").r(true).build();

              //  .build();

        ur.save(u);
        Assertions.assertThat(u.getId()).isGreaterThan(0);
    }
    @Test
    //@Rollback(value = false)
    @Order(2)
    public void getu(){
       utilisateur u=ur.findById(1).get();
        Assertions.assertThat(u.getId()).isEqualTo(1);
    }
    @Test
    //  @Rollback(value = false)
    @Order(3)
    public void getallu(){
List<utilisateur> u=  ur.findAll();
        Assertions.assertThat(u.size()).isGreaterThan(1);
    }
    @Test
    @Rollback(value = false)
    @Order(4)
    public void updateu(){
        utilisateur u =ur.findById(1).get();
        u.setEmail("ff@gmail.com");
        utilisateur uu = ur.save(u);
        Assertions.assertThat(uu.getEmail()).isEqualTo("ff@gmail.com");
    }
    @Test
    @Rollback(value = false)
    @Order(5)
    public void deleteu(){

        ur.deleteById(1);

        Assertions.assertThat(ur.findById(1)).isEmpty();

    }
}
