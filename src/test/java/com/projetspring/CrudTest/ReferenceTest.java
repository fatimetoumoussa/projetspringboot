package com.projetspring.CrudTest;

import com.projetspring.Model.PersonneMorale;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Model.Reference;
import com.projetspring.Repository.PersonneMoraleRepository;
import com.projetspring.Repository.PersonnePhisiqueRepository;
import com.projetspring.Repository.ReferenceRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ReferenceTest {

    @Autowired
    ReferenceRepository rr;
    @Autowired
    PersonnePhisiqueRepository phr;
   // PersonneMoraleRepository pmr;
    @Test
    @Rollback(value = false)
    @Order(1)
    public void  saveReference(){

        PersonnePhisique ph = phr.findById(1).get();
       // PersonneMorale pm = pmr.findById(1).get();

       Reference r= Reference.builder().entite("societe1")
               .datereference(LocalDate.parse("2022-04-22"))
               .montant(555).objet("rrrr").pp(ph)
                               .pm(null).build();

        rr.save(r);
        Assertions.assertThat(r.getId()).isGreaterThan(0);
    }
    @Test
    //@Rollback(value = false)
    @Order(2)
    public void getReference(){
        Reference r =rr.findById(1).get();
        Assertions.assertThat(r.getId()).isEqualTo(1);
    }
    @Test
    //  @Rollback(value = false)
    @Order(3)
    public void getallReference(){
        List<Reference> r =rr.findAll();
        Assertions.assertThat(r.size()).isGreaterThan(1);
    }

    @Test
    @Rollback(value = false)
    @Order(4)
    public void updateReference(){
        Reference r =rr.findById(1).get();
        r.setMontant(123);
        Reference ru = rr.save(r);
        Assertions.assertThat(ru.getMontant()).isEqualTo(123);
    }
    @Test
    @Rollback(value = false)
    @Order(5)
    public void deleteReference(){

        rr.deleteById(1);

        Assertions.assertThat(rr.findById(1)).isEmpty();

    }
}
