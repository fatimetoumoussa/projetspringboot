package com.projetspring.service;

import com.projetspring.Model.*;
import com.projetspring.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
public class AppelOffreService {
    @Autowired
     ReferenceRepository referenceRepository;

@Autowired
PersonneMoraleRepository personneMoraleRepository;
@Autowired
    AppelOffreRepository appelOffreRepository;


@Autowired
    PersonnePhisiqueRepository personnePhisiqueRepository;

@Autowired
    UtilisateurRepository utilisateurRepository;

    // ====== personne morale======



        public List<PersonneMorale> findAllPM(){

            return personneMoraleRepository.findAll();
        }

        public ResponseEntity<HttpStatus> deletePM(Integer id) {

                personneMoraleRepository.deleteById(id);
                return new ResponseEntity<>(null,HttpStatus.GONE);

        }
        public PersonneMorale savepm(PersonneMorale personneMorale){
            return personneMoraleRepository.save(personneMorale);
        }


    public PersonneMorale updatePm(PersonneMorale pm, Integer id) {

            pm.setId(id);
            personneMoraleRepository.save(pm);
            return pm;
            //return  serviceappeloffre.updatePH(pm,id);
    }
        public ResponseEntity<PersonneMorale> getpersonnepm(Integer id) {

            PersonneMorale pm = new PersonneMorale();
            pm = personneMoraleRepository.findById(id).get();
            return new ResponseEntity<>(pm, HttpStatus.FOUND);
        }



    //==============Reference


    public Reference saveR(Reference reference){
        referenceRepository.save(reference);
        return reference;
    }


    public List<Reference> findAllRef(){
        return referenceRepository.findAll();
    }

    public Reference getRef(Integer id){

        Reference ref=new Reference();
        ref=referenceRepository.findById(id).get();
        return  ref;
    }


    public Reference updater(Reference ref,Integer id) {
        Reference upr=new Reference();
       // Optional<Reference> phOptional = referenceRepository.findById(id);
        upr.setId(id);
        upr.setEntite(ref.getEntite());
        upr.setObjet(ref.getObjet());
        upr.setDatereference(ref.getDatereference());
        upr.setMontant(ref.getMontant());
        upr.setPp(ref.getPp());
        upr.setPm(ref.getPm());
         ;
        referenceRepository.save(upr);
         return upr;


    }
    public ResponseEntity<HttpStatus> deletereference(Integer id) {

            referenceRepository.deleteById(id);
            return new ResponseEntity<>(null,HttpStatus.GONE);

    }






//========apeel d'offre'


    public AppelOffre getAppelOffre(Integer id){

            AppelOffre ao=new AppelOffre();
            ao=appelOffreRepository.findById(id).get();
      return ao;
    }

    public AppelOffre saveAO(AppelOffre ao){
        return appelOffreRepository.save(ao);
    }
    @PutMapping("/updateAO/{id}")
    public AppelOffre updateAo(AppelOffre ao, Integer id) {
        ao.setId(id);

       // AppelOffre aou = appelOffreRepository.findById(id).get();
        return appelOffreRepository.save(ao);

    }

    public List<AppelOffre> findAllAO(){
        return appelOffreRepository.findAll();
    }
    @DeleteMapping("/deleteAO/{id}")
    public ResponseEntity<HttpStatus> deleteAO(Integer id) {

            appelOffreRepository.deleteById(id);
            return new ResponseEntity<>(null,HttpStatus.GONE);

    }

//==--------------personne phisique





    public PersonnePhisique saveph(PersonnePhisique pp) {

            return personnePhisiqueRepository.save(pp);
    }
    public PersonnePhisique getpersonneph(Integer id){

            return personnePhisiqueRepository.findById(id).get();
    }
    public List<PersonnePhisique> findAllPP(){

            return personnePhisiqueRepository.findAll();
    }

    public PersonnePhisique updatePH(PersonnePhisique ph, Integer id) {
        ph.setId(id);
       return personnePhisiqueRepository.save(ph);

    }



    public ResponseEntity<HttpStatus> deletePP(Integer id) {

            personnePhisiqueRepository.deleteById(id);
            return new ResponseEntity<>(null,HttpStatus.GONE);
    }

//user






    public utilisateur saveu(utilisateur u) {

        return utilisateurRepository.save(u);
    }
    public utilisateur getu(Integer id){

        return utilisateurRepository.findById(id).get();
    }
    public List<utilisateur> findAllu(){

        return utilisateurRepository.findAll();
    }

    public utilisateur updateu(utilisateur u, Integer id) {
        u.setId(id);
        return utilisateurRepository.save(u);

    }

    public ResponseEntity<HttpStatus> deleteu(Integer id) {

        utilisateurRepository.deleteById(id);
        return new ResponseEntity<>(null,HttpStatus.GONE);
    }



}
