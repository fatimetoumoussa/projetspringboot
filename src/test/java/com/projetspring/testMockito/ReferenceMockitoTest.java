package com.projetspring.testMockito;


import com.projetspring.Controller.AppelOffreController;
import com.projetspring.Model.PersonnePhisique;
import com.projetspring.Model.Reference;
import com.projetspring.service.AppelOffreService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ContextConfiguration(classes = {AppelOffreService.class, AppelOffreController.class})
@WebMvcTest
public class ReferenceMockitoTest {
    @Autowired
    MockMvc mkvc;
    @MockBean
    AppelOffreService appelOffreService;
  //@Mock
    PersonnePhisique ph;
   Reference ref = Reference.builder().entite("societe1")
                .datereference(LocalDate.parse("2022-04-22"))
            .montant(555).objet("rrrr").pp(ph)
                .pm(null).build();;
    private   Reference ref2;
  private List<Reference> listref=new ArrayList<>();



    @Test
    public void getallreference() throws Exception {
        ref=new Reference(1,"objet1","ref1", LocalDate.parse("2022-02-22"),444,null,null);
        listref.add(ref
        );
        when(appelOffreService.findAllRef()).thenReturn(listref);
mkvc.perform(get("/findAllRef")).andExpect(status().isOk()).andDo(print());

    }




    @Test
    public void getreference() throws Exception {
      //  ph=new PersonnePhisique(1,"nnn","sss","sss","sss","sss",null);
      // ref=new Reference(1,null,"lll", LocalDate.parse("2022-02-22"),444,ph,null);

        when(appelOffreService.getRef(ref.getId())).thenReturn(ref);
        mkvc.perform(get("/getRef/1")).andExpect(status().isOk()).andDo(print());

    }




    @Test
    public void postreference() throws Exception {

        mkvc.perform(post("/saveRef").content("    { \"objet\":\"referencetest\",\n" +
                "                \"entite\":\"GTITEST\",\n" +
                "                \"datereference\":\"2022-02-22\",\n" +
                "                \"montant\":444,\n" +
                "                \"pp\":{\n" +
                "            \"id\":1\n" +
                "        }\n" +
                "\n" +
                "\n" +
                "    }").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated()).andDo(print());

    }

    @Test
    public void deletereference() throws Exception {
        when(appelOffreService.deletereference(1)).thenReturn(null);
        mkvc.perform(delete("/deleteRef/1")).andExpect(status().isOk()).andDo(print());
    }







    @Test
    public void updater() throws Exception {

        mkvc.perform(put("/updateRef/1").content("    { \"objet\":\"referencetest\",\n" +
                "                \"entite\":\"GTITEST\",\n" +
                "                \"datereference\":\"2022-02-22\",\n" +
                "                \"montant\":444,\n" +
                "                \"pp\":{\n" +
                "            \"id\":1\n" +
                "        }\n" +
                "\n" +
                "\n" +
                "    }"
             ).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andDo(print());
    }

}
